$(function(){
    		$("[data-toggle='tooltip']").tooltip();
    		$("[data-toggle='popover']").popover();	
    		$('.carousel').carousel({
    			interval: 6000
    		});

    		$('#consultar').on('show.bs.modal', function (e){
    			console.log('el modal consultar se está mostrando');
  	  			$('contactoBtn').removeClass('btn-outline-success');
  	  			$('contactoBtn').addClass('btn-primary');
  	  			$('contactoBtn').prop('disabled', true);
    		});
    		$('#consultar').on('shown.bs.modal', function (e){
    			console.log('el modal consultar se mostró');
    		});
    		$('#consultar').on('hide.bs.modal', function (e){
    			console.log('el modal consultar se oculta');
    		});
    		$('#consultar').on('hidden.bs.modal', function (e){
    			console.log('el modal consultar se ocultó');
    			$('#consultarBtn').prop('disabled', false);
    		});

    	});